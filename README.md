##Demoprojekt zum Vortrag: Mutation Testing in Theorie und Praxis 






###Dieses Projekt enthält:

    -Calculator.java 



#####Zur Aktivierung des Pitest Mutation Framework für maven den Anweisungen in https://pitest.org/quickstart/maven/ folgen. Für JUnit5 muß als weiteres Plugin das _pitest-junit5-plugin_ eingebunden werden:



        <plugin>
          <groupId>org.pitest</groupId>
          <artifactId>pitest-maven</artifactId>
          <version>LATEST</version>

          <!-- Required for JUnit5 -->
          <dependencies>
            <dependency>
              <groupId>org.pitest</groupId>
              <artifactId>pitest-junit5-plugin</artifactId>
              <version>0.15</version>
            </dependency>
          </dependencies>
        </plugin>



#####Plugin Konfiguration mit erweiterten Einstellungen

        <plugin>
          <groupId>org.pitest</groupId>
          <artifactId>pitest-maven</artifactId>
          <version>LATEST</version>

          <configuration>
            <skip>${pit.skip}}</skip>
            <verbose>false</verbose>
            <mutators>
              <mutator>DEFAULTS</mutator>
              <mutator>MATH</mutator>
              <mutator>PRIMITIVE_RETURNS</mutator>
            </mutators>
            <targetClasses>
              <param>de.sidion.demo.mutationtesting.**</param>
            </targetClasses>
            <excludedClasses>
              <excludedClass>**.*Repository</excludedClass>
            </excludedClasses>
            <excludedMethods>
              <excludedMethod>toString</excludedMethod>
            </excludedMethods>
            <excludedTestClasses>
              <param>*IT</param>
            </excludedTestClasses>
            <avoidCallsTo>
              <avoidCallsTo>org.apache.log4j</avoidCallsTo>
            </avoidCallsTo>
            <outputFormats>
              <outputFormat>HTML</outputFormat>
              <outputFormat>XML</outputFormat>
            </outputFormats>
            <timestampedReports>false</timestampedReports >
          </configuration>





#####Selektive Ausführung für eine Klasse

    mvn test-compile pitest:mutationCoverage -DtargetClasses=de.sidion.demo.mutationtesting.calculator.Calculator

#####Selektive Ausführung für eine Klasse inklusive Einschränkung der zu verwendenden Testklasse

    mvn test-compile pitest:mutationCoverage -DtargetClasses=de.sidion.demo.mutationtesting.calculator.Calculator -DtargetTests=de.sidion.demo.mutationtesting.calculator.CalculatorTest

#####Initialer Lauf mit History file
    mvn pitest:mutationCoverage -Ppitest-history-setup

#####Delta Lauf mit History file
    mvn pitest:mutationCoverage -Ppitest-history-run    

#####Ergebnisse nach sonarqube senden
    mvn -Psonar

####Eine Linksammung rund um Mutation Testing findet ihr hier

https://github.com/theofidry/awesome-mutation-testing

